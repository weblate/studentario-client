import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: root

    property string subtitle: ""

    contentItem: Column {
        Text {
            id: titleControl
            anchors { left: parent.left; right: parent.right }
            text: root.text
            font: root.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
        }
        Label {
            anchors { left: parent.left; right: parent.right }
            text: root.subtitle
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            font {
                family: titleControl.font.family
                pixelSize: titleControl.font.pixelSize * 0.8
            }
        }
    }
}

