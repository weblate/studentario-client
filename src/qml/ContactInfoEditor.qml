import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ColumnLayout {
    id: root

    property var contactInfo: []
    property bool editable: false

    spacing: 4

    Repeater {
        model: root.contactInfo
        ContactInfoLine {
            Layout.fillWidth: true
            uri: modelData
            editable: root.editable
            onClicked: {
                editInfoPopup.contactUri = modelData
                editInfoPopup.targetIndex = index
                editInfoPopup.open()
            }
        }
    }

    Button {
        Layout.alignment: Qt.AlignHCenter
        text: qsTr("Adde un nove methodo de contacto")
        onClicked: newInfoPopup.open()
    }

    ContactInfoLinePopup {
        id: newInfoPopup
        implicitWidth: root.width * 3 / 4

        onSaveInfoRequested: {
            console.log("Adding URL: " + uri)
            var tmp = root.contactInfo
            tmp.push(uri)
            root.contactInfo = tmp
        }
    }

    ContactInfoLinePopup {
        id: editInfoPopup

        property int targetIndex: -1
        implicitWidth: root.width * 3 / 4

        onSaveInfoRequested: {
            console.log("Saving URL: " + uri)
            var tmp = root.contactInfo
            tmp[targetIndex] = uri
            root.contactInfo = tmp
        }
    }
}
