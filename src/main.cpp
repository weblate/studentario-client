/*
 * Copyright (C) 2021 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Studentario.
 *
 * Studentario is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Studentario is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"

#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    app.setOrganizationDomain("it.mardy");

    QTranslator translator;
    translator.load(QLocale(), QStringLiteral("studentario"),
                    QStringLiteral("_"), QStringLiteral(":/i18n"));
    app.installTranslator(&translator);

    Studentario::registerTypes();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/studentario.qml")));

    return app.exec();
}
